#!/usr/bin/python3
import random
import struct
import select
import socket
import subprocess
import configparser
import pdb
import time
import errno
import sys
import os

def chk(data):
    x = sum(x << 8 if i % 2 else x for i, x in enumerate(data)) & 0xFFFFFFFF
    x = (x >> 16) + (x & 0xFFFF)
    x = (x >> 16) + (x & 0xFFFF)
    return struct.pack('<H', ~x & 0xFFFF)


def ping(addr, timeout=1, number=1, data=b''):
    with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) as conn:
        payload = struct.pack('!HH', random.randrange(0, 65536), number) + data

        conn.connect((addr, 80))
        conn.sendall(b'\x08\0' + chk(b'\x08\0\0\0' + payload) + payload)
        start = time.time()

        while select.select([conn], [], [], max(0, start + timeout - time.time()))[0]:
            data = conn.recv(65536)
            if len(data) < 20 or len(data) < struct.unpack_from('!xxH', data)[0]:
                continue
            if data[20:] == b'\0\0' + chk(b'\0\0\0\0' + payload) + payload:
                return time.time() - start


def readini(INIFile):
        config = configparser.ConfigParser()
        config.read(INIFile)
        return config


def allowshutdown(hostlist):
    AllowShutDown = True
    for key, value in hostlist.items():
        print(timenow() + key + "-" + str(value))
        if value == True: # if at least one host is up we cannot shut down the NAS.
            return False
    return AllowShutDown

def timenow():
    return str("[" + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()) + "] ")


def getrtcwake():
    RTCValue = subprocess.run(["cat", "/sys/class/rtc/rtc0/wakealarm"], stdout=subprocess.PIPE)
    if str(RTCValue.stdout.decode()).replace('\n','') == '':
        print(timenow() + "No RTC Time set!")
        return {"isSet": False, "time": 0}
    else:
        epoch = int(str(RTCValue.stdout.decode()).replace('\n',''))
        print(timenow() + "RTCWake time is: " + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch)))
        return {"isSet": True, "time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch))}

if __name__ == '__main__':
    print('Shutdown Manager - v0.2 - Tal Ziv\n-----------------------------------\n')
    #pdb.set_trace()
    #sys.exit(0)
    checkrtc = getrtcwake()
    if checkrtc["isSet"] == False:
        print(timenow() + "No RTC time set, Setting now")
        RTCValue = subprocess.run(["/home/twhyman/rtcSet.sh"], stdout=subprocess.PIPE)
        print(str(RTCValue.stdout.decode()).replace('\n',''))
        #pdb.set_trace()
        sys.exit(0)
    workingDir = os.path.dirname(os.path.realpath(__file__))
    configuration = readini(workingDir + "/shut_nas.ini")
    hosts = configuration['config']['Hosts'].split(",")
    sleepTime = configuration['config']['Sleep']
    print("Working directory:" + workingDir + "/Shutdown_Manager.ini\nHosts: " + str(hosts) + "\nSleep time between checks: " + sleepTime + "\n\n")
    print("### Starting ###")
    HostStatus = {}
    while True:
        for host in hosts:
            print("Pinging: " + host)
            try:
                PingResult = ping(host)
            except PermissionError  as e:
                if (e.errno == errno.EPERM):
                    print("You need root permissions to this, exiting...", file=sys.stderr)
                    exit(1)

            if PingResult is None:
                HostStatus.update({host: False})
            else:
                HostStatus.update({host: True})

        shutdown = allowshutdown(HostStatus)
        if shutdown is True:
            print("Allow Susspend :)")
            subprocess.run(["systemctl", "suspend"])
            sys.exit(0)
        else:
            print("At least one host is Up,\nSleeping for 1 minute...")
            time.sleep(int(sleepTime))
